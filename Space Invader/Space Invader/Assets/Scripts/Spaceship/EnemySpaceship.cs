using System;

using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip enemyShoot;
        [SerializeField] private AudioClip enemyDead;
        [SerializeField] private double fireRate = 1;
        private float enemySound=0.5f;
        private float enemyDeadSound = 1f;
        private float fireCounter = 0;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(enemyDead,Camera.main.transform.position,enemyDeadSound);
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            //TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                AudioSource.PlayClipAtPoint(enemyShoot,Camera.main.transform.position,enemySound);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}