﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float chasingThresholdDistance;

        [SerializeField] private EnemySpaceship enemySpaceShip;
        [SerializeField] private EnemySpaceship enemySpaceShip2;
        [SerializeField] private PlayerSpaceship playerSpaceShip;

        private PlayerSpaceship spawnedPlayerShip;

        public void Init(PlayerSpaceship playerSpaceship)
        {
            spawnedPlayerShip = playerSpaceship;
        }

        private float speed = 2f;

        public void Update()
        {
            MoveToPlayer();
            ToPlayer();
            enemySpaceShip2.Fire();
            enemySpaceShip.Fire();
        }

         public void MoveToPlayer()
         {
             var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
           

             if (distanceToPlayer < chasingThresholdDistance)
             {
                 var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                 direction.Normalize();
                 var distance = direction * enemySpaceShip.Speed * Time.deltaTime;
                 gameObject.transform.Translate(distance);
             }
         }


         private void ToPlayer()
         {
             var distanceToPlayer2 = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
           

             if (distanceToPlayer2 < chasingThresholdDistance)
             {
                 var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                 direction.Normalize();
                 var distance = direction * enemySpaceShip2.Speed * Time.deltaTime;
                 gameObject.transform.Translate(distance);
             }
         }

    }
}


