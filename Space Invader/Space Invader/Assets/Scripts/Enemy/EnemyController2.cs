﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController2 : MonoBehaviour
    {
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private EnemySpaceship enemySpaceShip2;
        [SerializeField] private PlayerSpaceship playerSpaceShip;

        private PlayerSpaceship spawnedPlayerShip;

        public void Init(PlayerSpaceship playerSpaceship)
        {
            spawnedPlayerShip = playerSpaceship;
        }

        private float speed = 2f;

        private void Update()
        {
            ToPlayer();
            enemySpaceShip2.Fire();
        }

        private void ToPlayer()
        {
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
           

            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceShip2.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
        }

    }
}